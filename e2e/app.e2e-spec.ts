import { TalkhealthAssignmentPage } from './app.po';

describe('talkhealth-assignment App', () => {
  let page: TalkhealthAssignmentPage;

  beforeEach(() => {
    page = new TalkhealthAssignmentPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
