import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { CommonModule } from '@angular/common';
// import { ProductsListComponent } from './products-list/products-list.component';
import { ProductsService } from '../services/products.service'

@NgModule({
  imports: [
    CommonModule,
    HttpModule
  ],
  declarations: [
  	// ProductsListComponent, 
  	// ProductItemComponent
  ],
  providers: [
    ProductsService
  ]
})
export class ProductsModule { }
