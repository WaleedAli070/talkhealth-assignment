import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Product } from '../../models/product';
import { ProductsService } from '../../services/products.service';
import { ProductItemComponent } from './product-item/product-item.component'
import { Observable } from 'rxjs/Rx';
import { CartService } from '../../services/cart.service'

@Component({
  selector: 'app-products-list',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.css'],
  providers: [ ProductsService ]
})
export class ProductsListComponent implements OnInit {

	@Output() productSelected = new EventEmitter<Product>();
	products:Product[] = [];
	cart: Product[]
  	constructor(private ps: ProductsService, private cs: CartService) { 
  	this.ps.getProductItems().subscribe(
  			prod => {
  				for (var i = 0; i < prod.length; i++) {
  				  	this.products.push(prod[i])
  				}
  			}

  		);
  	}

  	ngOnInit() {
  		console.log(this.products)
  	}

  	onSelected(product: Product) {
  		this.productSelected.emit(product)
  	}

}
