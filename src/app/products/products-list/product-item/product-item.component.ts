import { Component, OnInit, Input } from '@angular/core';
import { Observable } from 'rxjs/Rx'
import { Product } from '../../../models/product';
import { User } from '../../../models/user';
import { CartService } from '../../../services/cart.service';
import { ProductsService } from '../../../services/products.service';
@Component({
  selector: 'app-product-item',
  templateUrl: './product-item.component.html',
  styleUrls: ['./product-item.component.css'],
  providers: [ ProductsService ]
})
export class ProductItemComponent implements OnInit {
	@Input() product: Product;
	
  	constructor(private cs: CartService) { 
  	}

	ngOnInit() {
		
	}
	addToCart(product: Product) {
		this.cs.addToCart(product)
	}

}
