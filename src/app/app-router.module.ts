import { Route } from '@angular/router';
import { HeaderComponent } from './header/header.component';
import { CartComponent } from './cart/cart.component';
import { ProductsListComponent } from './products/products-list/products-list.component';

export const appRoutes: Route[] = [
	{ path: '', redirectTo: 'Home', pathMatch: 'full'},
	{ path: 'Home', component: ProductsListComponent },
	{ path: 'Cart', component: CartComponent }
]

