import { Component, OnInit } from '@angular/core';
import { CartService } from '../services/cart.service';
import { Product } from '../models/product'
@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
  private cart:any
  private total:number = 0;
  constructor(private cs: CartService) { 
  	
  }

  ngOnInit() {
  	this.getCart();
  }
  getCart() {
  	this.cs.getCartItems().subscribe( updatedCart => {
  		this.cart = updatedCart;
  		for (var i = 0; i < this.cart.length; i++) {
  			this.total += this.cart[i].price_amount;
  		}
  	});
  }
  deleteItem(product: Product) {
  	this.cs.removeCartItem(product).subscribe( updatedCart => {
      this.cart = updatedCart;
      this.total = 0;
      for (var i = 0; i < this.cart.length; i++) {
          this.total += this.cart[i].price_amount;
        }
      }
      )
  }
}
