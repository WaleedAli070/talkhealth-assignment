import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router'

import { appRoutes } from './app-router.module';

import { AppComponent } from './app.component';
import { CartComponent } from './cart/cart.component';
import { ProductsListComponent } from './products/products-list/products-list.component';
import { ProductItemComponent } from './products/products-list/product-item/product-item.component';
import { HeaderComponent } from './header/header.component'


import { ProductsService } from './services/products.service';
import { CartService } from './services/cart.service';
@NgModule({
  declarations: [
    AppComponent,
    CartComponent,
    ProductsListComponent,
    ProductItemComponent,
    HeaderComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [
    CartService,
    ProductsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
