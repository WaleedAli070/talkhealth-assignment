export class Product {
	constructor(
		public title: string, 
		public image_url: string,
		public old_price: string,
		public price: string,
		public price_amount: number
		) {}
}

