import { Product } from './product'
export class User {
	constructor(public name: string, public cart: Product[] = [], public total_bill: number = 0, public items: number = 0) {}
}
