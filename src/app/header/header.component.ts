import { Component, OnInit } from '@angular/core';
import { Router, RouterModule } from '@angular/router';
import {Location, LocationStrategy, PathLocationStrategy} from '@angular/common';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
  providers: [Location, {provide: LocationStrategy, useClass: PathLocationStrategy}]
})
export class HeaderComponent implements OnInit {
	location: Location;
  constructor(location: Location) { 
  	this.location = location;
  	console.log(this.location)
  }

  ngOnInit() {
  }

}
