import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { User } from '../models/user';
import { Product } from '../models/product';
import { ProductsService } from '../services/products.service'
import { Observable } from 'rxjs/Rx'
import { BehaviorSubject } from 'rxjs/BehaviorSubject'

@Injectable()
export class CartService {
	cart: Product[] = [];
	private _cart: BehaviorSubject<Array<Product>>; 
	// private dataStore : {
	// 	cart: Product[]
	// }
	private products: Product[] = [];
  	constructor(private http:Http, private ps: ProductsService) { 
  		console.log("hello from cart service", this.cart)
  		// this.dataStore = { cart: [] }
  		this._cart = <BehaviorSubject<Array<Product>>>new BehaviorSubject(this.cart);
    	this.getCartFromLocalStorage();
  	}

  	addToCart(product: Product) {
  		console.log(this.cart)
  		this.cart.push(product);
        this._cart.next(this.cart);
        this.setCartInStorage();
  	}

  	getCartItems(): Observable<any> {
  		return this._cart.asObservable();
  	}

  	setCartInStorage() {
  		localStorage.setItem('cart',JSON.stringify(this.cart));
  	}
  	getCartFromLocalStorage() {
  		var cart = localStorage.getItem('cart');
  		if(!cart)
  			this.cart = [];
  		else {
  			this.cart = JSON.parse(cart);
  			this._cart.next(this.cart);
  		}
  	}
  	removeCartItem(product: Product): Observable<Product[]> {
  		var index = this.cart.indexOf(product)
  		this.cart.splice(index, 1);
        this._cart.next(this.cart);
        this.setCartInStorage();
        return this._cart.asObservable();

  	}
}
