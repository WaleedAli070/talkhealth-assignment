import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { Product } from '../models/product';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
@Injectable()
export class ProductsService {

  constructor(private http: Http) { 
  	
  }

  getProductItems(): Observable<Product[]> {
  	return this.http.get("./assets/products.json")
  					.map((response:any) => this.extractData(response))
  					.catch((error:any) => this.handleError(error));
  }
  extractData(response) {
  	let body = response.json();
  	console.log("hello from service", body.products[1])
    return body.products || {};
  }
  handleError(error) {
  	let errMsg = error.message || 'Server error';
    console.error(errMsg); // log to console instead
    return Observable.throw(errMsg);
  }

}
